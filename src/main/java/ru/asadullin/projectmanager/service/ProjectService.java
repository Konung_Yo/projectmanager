package ru.asadullin.projectmanager.service;

import ru.asadullin.projectmanager.dto.ProjectDto;
import ru.asadullin.projectmanager.entity.Project;

import java.util.List;

public interface ProjectService {
    List<Project> getProjectsHierarchy();

    List<Project> getAllProjects();

    Project getProjectById(Long id);

    Project updateProjectByDto(ProjectDto dto);

    void deleteProject(Long id);

    Project saveProjectByDto(ProjectDto projectDto);
}
