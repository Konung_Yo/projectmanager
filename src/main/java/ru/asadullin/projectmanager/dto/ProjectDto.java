package ru.asadullin.projectmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDto {
    @JsonProperty(value = "project_id")
    private Long id;
    @JsonProperty(value = "project_name")
    private String name;
    @JsonProperty(value = "project_parent")
    private Long projectParent;
}
