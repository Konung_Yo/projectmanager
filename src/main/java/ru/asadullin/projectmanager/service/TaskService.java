package ru.asadullin.projectmanager.service;

import ru.asadullin.projectmanager.dto.TaskDto;
import ru.asadullin.projectmanager.entity.Task;
import ru.asadullin.projectmanager.enums.Status;

import java.security.Principal;
import java.util.List;

public interface TaskService {
    Task saveTaskByDto(TaskDto dto, Principal principal);

    Task getTask(Long id);

    boolean deleteTask(Long id, Principal principal);

    Task updateTaskStatus(Long id, Status status);

    Task updateTaskByDto(TaskDto dto);

    List<Task> getAllTasks();
}
