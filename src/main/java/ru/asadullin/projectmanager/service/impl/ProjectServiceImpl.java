package ru.asadullin.projectmanager.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.asadullin.projectmanager.dto.ProjectDto;
import ru.asadullin.projectmanager.entity.Project;
import ru.asadullin.projectmanager.exception.EntityNotFoundException;
import ru.asadullin.projectmanager.repository.ProjectRepository;
import ru.asadullin.projectmanager.service.ProjectService;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    @Override
    @Transactional
    public List<Project> getProjectsHierarchy() {
        return projectRepository.findAllRoots();
    }

    @Override
    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    @Override
    public Project getProjectById(Long id) {
        return projectRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("There is no project with ID = " + id + " in Database."));
    }

    @Transactional
    @Override
    public Project updateProjectByDto(ProjectDto dto) {
        Project project = projectRepository.findById(dto.getId()).orElseThrow(() -> new EntityNotFoundException("There is no project with ID = " + dto.getId() + " in Database."));
        project.setProjectParent(projectRepository.getReferenceById(dto.getProjectParent()));
        project.setName(dto.getName());
        return projectRepository.save(project);
    }

    @Override
    public void deleteProject(Long id) {
        projectRepository.deleteById(id);
    }

    @Override
    public Project saveProjectByDto(ProjectDto projectDto) {
        Project project = Project.builder()
                .name(projectDto.getName())
                .createDate(LocalDateTime.now())
                .projectParent(projectRepository.getReferenceById(projectDto.getProjectParent()))
                .build();
        return projectRepository.save(project);
    }

}
