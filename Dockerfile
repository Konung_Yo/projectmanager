FROM adoptopenjdk/openjdk11:alpine-jre
ARG JAR_FILE=target/ProjectManager-0.0.1-SNAPSHOT.jar
WORKDIR /opt/app
COPY ${JAR_FILE} ProjectManager.jar
ENTRYPOINT ["java","-jar","ProjectManager.jar"]



