package ru.asadullin.projectmanager.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.asadullin.projectmanager.dto.ProjectDto;
import ru.asadullin.projectmanager.entity.Project;
import ru.asadullin.projectmanager.service.ProjectService;

import java.util.List;

@RestController
@RequestMapping("/api/project")
@RequiredArgsConstructor
public class ProjectController {
    private final ProjectService projectService;

    @GetMapping("/getHierarchy")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public List<Project> getAllProjectsWithHierarchy() {
        return projectService.getProjectsHierarchy();
    }

    @PostMapping("/post")
    @PreAuthorize("hasRole('ADMIN')")
    public Project addNewProject(@RequestBody ProjectDto projectDto) {
        return projectService.saveProjectByDto(projectDto);
    }

    @GetMapping("/get/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Project getProject(@PathVariable Long id) {
        return projectService.getProjectById(id);
    }

    @PutMapping("/put")
    @PreAuthorize("hasRole('ADMIN')")
    public Project update(@RequestBody ProjectDto dto) {
        return projectService.updateProjectByDto(dto);
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteProject(@PathVariable Long id) {
        projectService.deleteProject(id);
    }
}
