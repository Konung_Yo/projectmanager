package ru.asadullin.projectmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.asadullin.projectmanager.entity.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Query("SELECT project FROM Project project WHERE project.projectParent.id IS NULL")
    List<Project> findAllRoots();

    Project findProjectById(Long id);
}
