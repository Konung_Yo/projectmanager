package ru.asadullin.projectmanager.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.asadullin.projectmanager.dto.TaskDto;
import ru.asadullin.projectmanager.entity.Task;
import ru.asadullin.projectmanager.enums.Status;
import ru.asadullin.projectmanager.service.TaskService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/task")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @PostMapping("/post")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public Task addNewTask(@RequestBody TaskDto dto, Principal principal) {
        return taskService.saveTaskByDto(dto, principal);
    }

    @GetMapping("/get/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Task getTask(@PathVariable Long id) {
        return taskService.getTask(id);
    }

    @GetMapping("/getAll")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public List<Task> getAllTasks() {
        return taskService.getAllTasks();
    }

    @PutMapping("/statusUpdate/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public Task updateTaskStatus(@PathVariable("id") Long id, @RequestBody Status status) {
        return taskService.updateTaskStatus(id, status);
    }

    @PutMapping("/put")
    @PreAuthorize("hasRole('ADMIN')")
    public Task updateTask(@RequestBody TaskDto dto) {
        return taskService.updateTaskByDto(dto);
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public HttpStatus deleteTask(@PathVariable Long id, Principal principal) {
        final boolean updated = taskService.deleteTask(id, principal);
        return updated ? HttpStatus.OK : HttpStatus.NOT_MODIFIED;
    }
}
