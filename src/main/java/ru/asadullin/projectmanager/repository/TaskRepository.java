package ru.asadullin.projectmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.asadullin.projectmanager.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}
