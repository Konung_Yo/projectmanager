package ru.asadullin.projectmanager.enums;

public enum RoleType{
    ROLE_USER,
    ROLE_ADMIN
}
