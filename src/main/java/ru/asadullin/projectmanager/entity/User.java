package ru.asadullin.projectmanager.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.asadullin.projectmanager.enums.RoleType;

import javax.persistence.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name="users")
public class User {
    @JsonProperty(value = "user_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonProperty(value = "user_name")
    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @JsonProperty(value = "user_password")
    @Column(name = "user_password", nullable = false)
    private String userPassword;

    @JsonProperty(value = "user_roleType")
    @Column(name = "role_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

}
