package ru.asadullin.projectmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import ru.asadullin.projectmanager.enums.RoleType;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    @JsonProperty(value = "username")
    private String username;
    @JsonProperty(value = "password")
    private String password;
    @JsonProperty(value = "role_type")
    private RoleType roleType;
}
