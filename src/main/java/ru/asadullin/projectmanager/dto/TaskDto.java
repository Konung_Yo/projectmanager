package ru.asadullin.projectmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import ru.asadullin.projectmanager.enums.TaskType;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto {
    @JsonProperty(value = "task_id")
    private Long id;
    @JsonProperty(value = "task_name")
    private String taskName;
    @JsonProperty(value = "task_description")
    private String description;
    @JsonProperty(value = "project_id")
    private Long projectId;
    @JsonProperty(value = "task_type")
    private TaskType taskType;
}
