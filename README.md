# Project Manager

### Описание проекта

Project Manager - RESTful веб-приложение для работы с проектами/подпроектами и задачами. 

### Стек технологий

```
IntelliJ IDEA Ultimate
Java SE Development 11
Apache Maven version 4.0.0
Git
Spring Boot
Spring WebMVC
Spring Data JPA
Spring Security
Jackson
Flyway
H2
Lombok
Postman
```

### Запуск приложения:
```
Клонировать репозиторий, запустить проект. На проекте используется база H2 in-memory, Первичные, тестовые данные будут подгружены, миграция данных осуществлена с помощью Flyway.
Postman коллекция с методами для взаимодействия с приложением в файле: Project manager Collection.postman_collection.
В качестве авторизации использованы Spring Security, JWT токен.
```
### Авторизация:
```
Для авторизации пользователя используется комманда в коллекции - Sign in.
Представлены следующие логины:
- admin Пароль: password
- user Пароль: password
- user1 Пароль: password
При удачном логине присылается токен, его нужно использовать во вкладке "Authorization" в постмане, выставив тип "Bearer Token" и вставив в поле "Token".
```
### Описание:
```
Требования и задачи по проекту описаны в task.md.
```
### Разработчик

```
Асадуллин Эльнур Маратович
Asadelmar@gmail.com
```