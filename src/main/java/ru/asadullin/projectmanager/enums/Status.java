package ru.asadullin.projectmanager.enums;

public enum Status {
    NEW,
    PROGRESS,
    DONE
}
