package ru.asadullin.projectmanager.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.asadullin.projectmanager.dto.UserDto;
import ru.asadullin.projectmanager.entity.User;
import ru.asadullin.projectmanager.repository.UserRepository;
import ru.asadullin.projectmanager.service.UserService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    @Transactional
    public User saveOrUpdate(UserDto userDto) {
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User user = User.builder().username(userDto.getUsername())
                .userPassword(userDto.getPassword())
                .roleType(userDto.getRoleType()).build();
        return userRepository.save(user);
    }
}
