package ru.asadullin.projectmanager.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.asadullin.projectmanager.component.JwtTokenUtil;
import ru.asadullin.projectmanager.dto.JwtResponse;
import ru.asadullin.projectmanager.dto.UserDto;
import ru.asadullin.projectmanager.entity.User;
import ru.asadullin.projectmanager.enums.RoleType;
import ru.asadullin.projectmanager.exception.EntityNotFoundException;
import ru.asadullin.projectmanager.exception.IncorrectDataMessage;
import ru.asadullin.projectmanager.service.UserService;
import ru.asadullin.projectmanager.service.impl.JpaUserDetailsService;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final JpaUserDetailsService jpaUserDetailsService;
    private final UserService userService;
    private final JwtTokenUtil jwtTokenUtil;

    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.CREATED)
    public User saveNewUser(@RequestBody UserDto userDto) {
        return userService.saveOrUpdate(userDto);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> createAuthToken(@RequestBody UserDto userDto) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userDto.getUsername(), userDto.getPassword()));
        } catch (BadCredentialsException ex) {
            return new ResponseEntity<>(new IncorrectDataMessage(HttpStatus.UNAUTHORIZED.value(), "Incorrect username or password"), HttpStatus.UNAUTHORIZED);
        }
        UserDetails userDetails = jpaUserDetailsService.loadUserByUsername(userDto.getUsername());
        String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token, userExistsRoleAdmin(userDto.getUsername())));
    }

    public boolean userExistsRoleAdmin(String username) {
        User user = userService.findByUsername(username).orElseThrow(() ->
                new EntityNotFoundException("User with username: " + username + " does not exist"));
        return user.getRoleType().equals(RoleType.ROLE_ADMIN);
    }
}
