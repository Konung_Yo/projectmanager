package ru.asadullin.projectmanager.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import ru.asadullin.projectmanager.enums.Status;
import ru.asadullin.projectmanager.enums.TaskType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "task")
public class Task {
    @JsonProperty(value = "task_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonProperty(value = "task_name")
    @Column(name = "task_name", nullable = false)
    private String taskName;

    @JsonProperty(value = "task_description")
    @Column(name = "description")
    private String description;

    @JsonProperty(value = "user_id")
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @JsonProperty(value = "task_status")
    @Column(name = "task_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @JsonProperty(value = "task_type")
    @Column(name = "task_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private TaskType taskType;

    @JsonProperty(value = "create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Column(name = "create_date", nullable = false)
    private LocalDateTime createDate;

    @JsonProperty(value = "status_change_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Column(name = "status_change_date", nullable = false)
    private LocalDateTime statusChangeDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    @JsonIgnore
    private Project project;
}
