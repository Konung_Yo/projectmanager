package ru.asadullin.projectmanager.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.asadullin.projectmanager.dto.TaskDto;
import ru.asadullin.projectmanager.entity.Task;
import ru.asadullin.projectmanager.entity.User;
import ru.asadullin.projectmanager.enums.RoleType;
import ru.asadullin.projectmanager.enums.Status;
import ru.asadullin.projectmanager.exception.EntityNotFoundException;
import ru.asadullin.projectmanager.repository.ProjectRepository;
import ru.asadullin.projectmanager.repository.TaskRepository;
import ru.asadullin.projectmanager.repository.UserRepository;
import ru.asadullin.projectmanager.service.TaskService;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final ProjectRepository projectRepository;

    @Override
    @Transactional
    public Task saveTaskByDto(TaskDto dto, Principal principal) {
        final User user = userRepository.findByUsername(principal.getName())
                .orElseThrow((() -> new UsernameNotFoundException("Username not found: " + principal.getName())));
        Task task = Task.builder().taskName(dto.getTaskName())
                .description(dto.getDescription())
                .userId(user.getId())
                .status(Status.NEW)
                .taskType(dto.getTaskType())
                .createDate(LocalDateTime.now())
                .statusChangeDate(LocalDateTime.now())
                .project(projectRepository.getReferenceById(dto.getProjectId()))
                .build();
        return taskRepository.save(task);
    }

    @Override
    public Task getTask(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("There is no task with ID = " + id + " in Database."));
    }

    @Override
    @Transactional
    public boolean deleteTask(Long id, Principal principal) {
        final User user = userRepository.findByUsername(principal.getName())
                .orElseThrow((() -> new UsernameNotFoundException("Username not found: " + principal.getName())));
        final Task task = taskRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("There is no task with ID = " + id + " in Database."));
        if (!user.getRoleType().equals(RoleType.ROLE_ADMIN)) { //здесь мы узнаем не админ ли пользователь
            if (task.getUserId().equals(user.getId())) { //если пользователь не админ, узнаем он ли создатель задачи
                taskRepository.deleteById(id);
                return true;
            } else {
                return false;
            }
        } else {
            taskRepository.deleteById(id);
            return true;
        }
    }

    @Override
    @Transactional
    public Task updateTaskStatus(Long id, Status status) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("There is no task with ID = " + id + " in Database"));
        task.setStatus(status);
        task.setStatusChangeDate(LocalDateTime.now());
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public Task updateTaskByDto(TaskDto dto) {
        Task task = taskRepository.findById(dto.getId())
                .orElseThrow(() -> new EntityNotFoundException("There is no task with ID = " + dto.getId() + " in Database"));
        task.setTaskName(dto.getTaskName());
        task.setDescription(dto.getDescription());
        task.setProject(projectRepository.getReferenceById(dto.getProjectId()));
        task.setTaskType(dto.getTaskType());
        return taskRepository.save(task);
    }

    @Override
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }
}
