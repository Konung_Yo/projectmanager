package ru.asadullin.projectmanager.service;

import ru.asadullin.projectmanager.dto.UserDto;
import ru.asadullin.projectmanager.entity.User;

import java.util.Optional;

public interface UserService {
    Optional<User> findByUsername(String username);
    User saveOrUpdate(UserDto userDto);
}
