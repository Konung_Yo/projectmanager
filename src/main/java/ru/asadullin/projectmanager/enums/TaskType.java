package ru.asadullin.projectmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TaskType {
    MANAGER("Менеджер"),
    TECH_SPECIALIST("Технический специалист");
    private final String description;
}
